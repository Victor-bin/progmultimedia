﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace _02_AplicationMethods
{
    public partial class FormMetodos : Form
    {
        public FormMetodos()
        {
            InitializeComponent();
        }

        private int SumaEntrada(int n1, int n2)
        {
            int res = n1 + n2;
            n1 = 10;
            n2 = 20;
            return res;
        }

        private int SumaES(ref int n1, ref int n2)
        {
            int res = n1 + n2;
            n1 = 10;
            n2 = 20;
            return res;
        }

        private void SumaSalida(int n1, int n2,out int res)
        {
            res = n1 + n2;
            n1 = 10;
            n2 = 20;
            return;
        }

        private void SumaMetodoEntrada_Click(object sender, EventArgs e)
        {
            int num1, num2, res;
            num1 = Int32.Parse(textBoxNum1.Text);
            num2 = Int32.Parse(textBoxNum2.Text);
            res = SumaEntrada(num1, num2);

            Resultado.Text = "La suma de " + num1 + " y " + num2 + " es:" + res;
        }

        private void SumaMetodoES_Click(object sender, EventArgs e)
        {
            int num1, num2, res;
            num1 = Int32.Parse(textBoxNum1.Text);
            num2 = Int32.Parse(textBoxNum2.Text);
            res = SumaES(ref num1, ref num2);

            Resultado.Text = "La suma de " + num1 + " y " + num2 + " es:" + res;
        }

        private void SumaMetodoSalida_Click(object sender, EventArgs e)
        {
            int num1, num2, res;
            num1 = Int32.Parse(textBoxNum1.Text);
            num2 = Int32.Parse(textBoxNum2.Text);
            SumaSalida(num1, num2, out res);

            Resultado.Text = "La suma de " + num1 + " y " + num2 + " es:" + res;
        }

        private void sumarClase(Persona p, int valor ) {
            p.setEdad(p.getEdad() + valor);
        }

        private void buttonSumaMetodoClase_Click ( object sender, EventArgs e ) {
            Persona p = new Persona ("a",10);
            int num1 = Int32.Parse (textBoxNum1.Text);

            sumarClase (p, num1);

            Resultado.Text = "La suma es "+ p.getEdad();
        }
    }
}
