﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _02_AplicationMethods {
    class Persona {
        private string nombre;
        private int edad; //Por defecto es private

        public Persona(string nombre, int edad ) {
            this.edad = edad;
            this.nombre = nombre;
        }


        public void setEdad(int nueva) {
            edad = nueva;
        }

        public int getEdad () {
            return edad;
        }
    }
}
