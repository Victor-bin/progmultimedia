﻿namespace _02_AplicationMethods
{
    partial class FormMetodos
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.SumaMetodoEntrada = new System.Windows.Forms.Button();
            this.SumaMetodoES = new System.Windows.Forms.Button();
            this.SumaMetodoSalida = new System.Windows.Forms.Button();
            this.numero1 = new System.Windows.Forms.Label();
            this.numero2 = new System.Windows.Forms.Label();
            this.textBoxNum1 = new System.Windows.Forms.TextBox();
            this.textBoxNum2 = new System.Windows.Forms.TextBox();
            this.Resultado = new System.Windows.Forms.TextBox();
            this.ResultadoTittle = new System.Windows.Forms.Label();
            this.buttonSumaMetodoClase = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // SumaMetodoEntrada
            // 
            this.SumaMetodoEntrada.Location = new System.Drawing.Point(445, 23);
            this.SumaMetodoEntrada.Name = "SumaMetodoEntrada";
            this.SumaMetodoEntrada.Size = new System.Drawing.Size(131, 44);
            this.SumaMetodoEntrada.TabIndex = 0;
            this.SumaMetodoEntrada.Text = "Suma Metodo Entrada";
            this.SumaMetodoEntrada.UseVisualStyleBackColor = true;
            this.SumaMetodoEntrada.Click += new System.EventHandler(this.SumaMetodoEntrada_Click);
            // 
            // SumaMetodoES
            // 
            this.SumaMetodoES.Location = new System.Drawing.Point(445, 94);
            this.SumaMetodoES.Name = "SumaMetodoES";
            this.SumaMetodoES.Size = new System.Drawing.Size(131, 44);
            this.SumaMetodoES.TabIndex = 0;
            this.SumaMetodoES.Text = "Suma Metodo E/S";
            this.SumaMetodoES.UseVisualStyleBackColor = true;
            this.SumaMetodoES.Click += new System.EventHandler(this.SumaMetodoES_Click);
            // 
            // SumaMetodoSalida
            // 
            this.SumaMetodoSalida.Location = new System.Drawing.Point(445, 170);
            this.SumaMetodoSalida.Name = "SumaMetodoSalida";
            this.SumaMetodoSalida.Size = new System.Drawing.Size(131, 44);
            this.SumaMetodoSalida.TabIndex = 0;
            this.SumaMetodoSalida.Text = "Suma Metodo Salida";
            this.SumaMetodoSalida.UseVisualStyleBackColor = true;
            this.SumaMetodoSalida.Click += new System.EventHandler(this.SumaMetodoSalida_Click);
            // 
            // numero1
            // 
            this.numero1.AutoSize = true;
            this.numero1.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F);
            this.numero1.Location = new System.Drawing.Point(12, 64);
            this.numero1.Name = "numero1";
            this.numero1.Size = new System.Drawing.Size(108, 26);
            this.numero1.TabIndex = 1;
            this.numero1.Text = "Numero1:";
            // 
            // numero2
            // 
            this.numero2.AutoSize = true;
            this.numero2.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F);
            this.numero2.Location = new System.Drawing.Point(12, 138);
            this.numero2.Name = "numero2";
            this.numero2.Size = new System.Drawing.Size(108, 26);
            this.numero2.TabIndex = 1;
            this.numero2.Text = "Numero2:";
            // 
            // textBoxNum1
            // 
            this.textBoxNum1.Location = new System.Drawing.Point(126, 70);
            this.textBoxNum1.Name = "textBoxNum1";
            this.textBoxNum1.Size = new System.Drawing.Size(226, 20);
            this.textBoxNum1.TabIndex = 2;
            // 
            // textBoxNum2
            // 
            this.textBoxNum2.Location = new System.Drawing.Point(126, 144);
            this.textBoxNum2.Name = "textBoxNum2";
            this.textBoxNum2.Size = new System.Drawing.Size(226, 20);
            this.textBoxNum2.TabIndex = 2;
            // 
            // Resultado
            // 
            this.Resultado.Enabled = false;
            this.Resultado.Location = new System.Drawing.Point(17, 299);
            this.Resultado.Name = "Resultado";
            this.Resultado.Size = new System.Drawing.Size(573, 20);
            this.Resultado.TabIndex = 3;
            this.Resultado.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // ResultadoTittle
            // 
            this.ResultadoTittle.AutoSize = true;
            this.ResultadoTittle.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F);
            this.ResultadoTittle.Location = new System.Drawing.Point(214, 256);
            this.ResultadoTittle.Name = "ResultadoTittle";
            this.ResultadoTittle.Size = new System.Drawing.Size(165, 29);
            this.ResultadoTittle.TabIndex = 4;
            this.ResultadoTittle.Text = "RESULTADO:";
            // 
            // buttonSumaMetodoClase
            // 
            this.buttonSumaMetodoClase.Location = new System.Drawing.Point(445, 220);
            this.buttonSumaMetodoClase.Name = "buttonSumaMetodoClase";
            this.buttonSumaMetodoClase.Size = new System.Drawing.Size(131, 44);
            this.buttonSumaMetodoClase.TabIndex = 0;
            this.buttonSumaMetodoClase.Text = "Suma Metodo clase";
            this.buttonSumaMetodoClase.UseVisualStyleBackColor = true;
            this.buttonSumaMetodoClase.Click += new System.EventHandler(this.buttonSumaMetodoClase_Click);
            // 
            // FormMetodos
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.CornflowerBlue;
            this.ClientSize = new System.Drawing.Size(602, 340);
            this.Controls.Add(this.ResultadoTittle);
            this.Controls.Add(this.Resultado);
            this.Controls.Add(this.textBoxNum2);
            this.Controls.Add(this.textBoxNum1);
            this.Controls.Add(this.numero2);
            this.Controls.Add(this.numero1);
            this.Controls.Add(this.buttonSumaMetodoClase);
            this.Controls.Add(this.SumaMetodoSalida);
            this.Controls.Add(this.SumaMetodoES);
            this.Controls.Add(this.SumaMetodoEntrada);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FormMetodos";
            this.Text = "Metodos C#";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button SumaMetodoEntrada;
        private System.Windows.Forms.Button SumaMetodoES;
        private System.Windows.Forms.Button SumaMetodoSalida;
        private System.Windows.Forms.Label numero1;
        private System.Windows.Forms.Label numero2;
        private System.Windows.Forms.TextBox textBoxNum1;
        private System.Windows.Forms.TextBox textBoxNum2;
        private System.Windows.Forms.TextBox Resultado;
        private System.Windows.Forms.Label ResultadoTittle;
        private System.Windows.Forms.Button buttonSumaMetodoClase;
    }
}

