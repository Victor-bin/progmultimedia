# Introducción a c\# #
Proviene de c++ que a su vez viene de C

## Ventajas respecto a java
- Interoperabilidad de lenguajes
- La integración de Java con plataforma Windows es menor

## Características
- Orientado a objetos y componentes
- Gestión automática y dinámica de la memoria
- Seguridad de tipos
- Sistemas de tipos unificado
- Extensibilidad de operadores

## Comentarios
c\# permite comentarios de una o varias líneas

```c#
/* Comentario
de
varias
lineas */

// Comentario de una linea
```
## Identificadores
- Se admiten: Mayúsculas, minúsculas, dígitos y barra baja (_).
- **NO** puede comenzar por dígitos.
- **NO** puede incluir espacios en blanco.
- **NO** puede ser una palabra reservada.
- Es sensible a mayúsculas y minúsculas.


## Palabras reservadas
|  |  |  |  |  |  |  |
| -- | -- | -- | -- | -- | -- | -- |
| abstract | as | base | bool | break | byte | case |
| catch | char | checked | class | const | continue | decimal |
| default | delegate | do | double | else | enum | event |
| explicit | extern | false | finally | fixed | float | for |
| foreach | goto | if | implicit | in | int | interface |
| internal | is | lock | long | namespace | new | null |
| object | operator | out | override | params | private | protected |
| public | readonly | ref | return | sbyte | sealed | short |
| sizeof | stackalloc | static | string | struct | switch | this |
| throw | true | try | typeof | uint | ulong | unchecked |
| unsafe | ushort | using | virtual | void | volatile | while |

## Recomendación para variables
- No utilizar __
- No crear identificadores que sólo se diferencien por mayúsculas / minúsculas
- Comenzar por minúscula 
- En un identificador de varias palabras: comenzar la segunda y sucesiva por letra Mayúscula 
- No utilizar notación húngara

## Declaración
Declaración simple:

\< tipo de dato \> \< identificador \>;

```c#
 int numero1;
```
Declaración múltiple:

\< tipo de dato \> \< identificador1 \>, \< identificador2 \>,,,,;

```c#
 int numero1, numero2, numero3;
```
Declaración e inicialización:

\< tipo de dato \> \< identificador \> = \< valor \>;

```c#
 int numero1 = 7;
``` 
## Tipos de datos
 - *int*: Enteros
 - *long*: Enteros largos
 - *float*: Reales
 - *double*: Reales largos
 - *decimal*: Moneda
 - *string*: cadena de caracteres
 - *char*: carácter
 - *bool*: lógico / booleano
 
## Operadores
### Aritméticos
 - Suma: \+
 - Resta: \-
 - Producto: \*
 - División: /
 - Resto: %
 - Incremento: \+\+
 - Decremento: \-\-
 
### Lógicos
 - AND: \&\&
 - OR: ||
 - NOT: \!

### Relacionales
 - Asignación: ==
 - Distinto: \!=
 - Menor: \<
 - Menor o igual: <=
 - Mayor: \>
 - Mayor o igual: >=
 
### Manipulación de bits
 - AND: \&
 - OR: |
 - XOR: ^
 - Complemento a 1: ~
 - Rotación izquierda: <<
 - Rotación derecha: >>

### Asignación
 - Suma: \+=
 - Resta: \-=
 - Producto: \*=
 - División: /=
 - Resto: %=
 - AND: \&=
 - OR: |=
 - XOR: ^=
 - Complemento a 1: ~=
 - Rotación izquierda: <<=
 - Rotación derecha: >>=
 
### Cadenas
 - Suma: \+
 - Resta: \-
 - Producto: \*
 - División: /
 - Resto: %
 - Incremento: \+\+
 - Decremento: \-\-

### Operador condicional
Similar a un if 

\< condición \> ? \< expresión si la condición se cumple \> : \< expresión si no se cumple \> ;

```c#
 Nota >= 5 ? Aprobado = false : Aprobado = true;
``` 
### Operadores sobre tipos
- typeof( \< tipo \> )
-  \< expresión \> is  \< tipo \> 
- sizeof( \< tipo \> )
### Operadores de conversión
- ( \< tipo a convertir \> ) \< expresión \>
-  \< expresión \> as  \< tipo a convertir \>
