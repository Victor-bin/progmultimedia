using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApplicationBucles {
    public partial class Form1 :Form {
        public Form1 () {
            InitializeComponent ();
        }

        private List<int> adquirirNumeros (TextBox a, TextBox b)
        {
            List<int> numeros = new List<int>();
            try
            {
                numeros.Add(Int32.Parse(a.Text));
                numeros.Add(Int32.Parse(b.Text));
                return numeros;
            }
            catch (Exception)
            {
                Console.WriteLine("ERCaracteres erroneos o inexistentes");
                throw;
            }
        } 

        private void buttonSuma_Click ( object sender, EventArgs e ) {
            //int[] tabla = { 11, 22, 33, 44 };
            //labelResultado.Text = "";
            //foreach (int numero in tabla){
            //    labelResultado.Text +="\nNumero: "+ numero;
            //}
            //int n1, n2;
            //n1 = Int32.Parse (textBoxNumero1.Text);
            //n2 = Int32.Parse (textBoxNumero2.Text);
            //labelResultado.Text = "La suma de " + n1 + " y " + n2 + "  es: " + (n1 + n2);
            try
            {
                List<int> argumento = adquirirNumeros(textBoxNumero1, textBoxNumero2);
                labelResultado.ForeColor = Color.Black;
                labelResultado.Text = argumento[0] + " + " + argumento[1] + "  es: " + (argumento[0] + argumento[1]);
            }
            catch (Exception)
            {
                labelResultado.ForeColor = Color.Red;
                labelResultado.Text = "ARGUMENTOS INVALIDOS";
            }
        }

        private void buttonDiv_Click(object sender, EventArgs e)
        {
            //int n1, n2;
            //n1 = Int32.Parse(textBoxNumero1.Text);
            //n2 = Int32.Parse(textBoxNumero2.Text);
            //labelResultado.Text = "La division de " + n1 + " y " + n2 + "  es: " + (n1 / n2);
            try
            {
                List<int> argumento = adquirirNumeros(textBoxNumero1, textBoxNumero2);
                labelResultado.ForeColor = Color.Black;
                labelResultado.Text = argumento[0] + " ÷ " + argumento[1] + "  es: " + (argumento[0] / argumento[1]);
            }
            catch (Exception)
            {
                labelResultado.ForeColor = Color.Red;
                labelResultado.Text = "ARGUMENTOS INVALIDOS";
            }
        }

        private void buttonResta_Click(object sender, EventArgs e)
        {
            //int n1, n2;
            //n1 = Int32.Parse(textBoxNumero1.Text);
            //n2 = Int32.Parse(textBoxNumero2.Text);
            //labelResultado.Text = "La resta de " + n1 + " y " + n2 + "  es: " + (n1 - n2);
            try
            {
                List<int> argumento = adquirirNumeros(textBoxNumero1, textBoxNumero2);
                labelResultado.ForeColor = Color.Black;
                labelResultado.Text = argumento[0] + " - " + argumento[1] + "  es: " + (argumento[0] - argumento[1]);
            }
            catch (Exception)
            {
                labelResultado.ForeColor = Color.Red;
                labelResultado.Text = "ARGUMENTOS INVALIDOS";
            }
        }

        private void buttonMulti_Click(object sender, EventArgs e)
        {
            //int n1, n2;
            //n1 = Int32.Parse(textBoxNumero1.Text);
            //n2 = Int32.Parse(textBoxNumero2.Text);
            //labelResultado.Text = "La multiplicacion de " + n1 + " y " + n2 + "  es: " + (n1 * n2);
            try
            {
                List<int> argumento = adquirirNumeros(textBoxNumero1, textBoxNumero2);
                labelResultado.ForeColor = Color.Black;
                labelResultado.Text = argumento[0] + " X " + argumento[1] + "  es: " + (argumento[0] * argumento[1]);
            }
            catch (Exception)
            {
                labelResultado.ForeColor = Color.Red;
                labelResultado.Text = "ARGUMENTOS INVALIDOS";
            }
        }
        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

    }
}
