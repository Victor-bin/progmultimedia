# .NET

Se trata de una plataforma no de un lenguaje o entorno.

## Esta formado por:

- Un entorno de ejecución *(Runtime)*
- Biblioteca de clases *(Class Library)*
- lenguajes de programación
- compiladores
- Herramientas de desarrollo *(IDE y herramientas)*

## Características:

- Plataforma de ejecución intermedia.
- Orientado a objetos.
- Multilenguaje.

## .NET Framework

Incluye:

- Un entorno de ejecución *(Runtime)*
- Biblioteca de clases *(Class Library)*

