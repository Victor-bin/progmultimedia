﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication1
{
    class Program
    {
        static void Main(string[] args)
        {
            int i;
            double d;
            i = Truncar(10.2);
            Console.WriteLine(i + "/n");
            Truncar(14.89, out i);
            Console.WriteLine(i + "/n");

        }

        /// <summary>
        ///  Un parámetro de tipo real y devuelve el número tras truncar la parte decimal a 0 decimales.
        /// </summary>
        /// <example> 764.783 -> 764.78 </example>
        /// <param name="num">El numero a truncar</param>
        /// <returns> Devuelve el número tras truncar la parte decimal a 0 decimales </returns>
        static public int Truncar(double num)
        {
            return (int)Math.Truncate(num);

        }


        /// <summary>
        /// Un parámetro de tipo real y otro parámetro de tipo entero.El método almacena sobre el segundo parámetro el número tras truncar la parte decimal a 0 decimales.
        /// </summary>
        /// <example> 764.783 -> 764.78 </example> 
        /// <param name="num">El numero a truncar</param>
        /// <param name="resut">Contendra el numero ya truncado</param>
        /// <returns> void </returns>
        static public void Truncar(double num, out int resut)
        {
            resut = (int)Math.Truncate(num);

        }

        /// <summary>
        /// Un parámetro de tipo real.El método almacena sobre el primer parámetro el número tras truncar la parte decimal a 0 decimales.
        /// </summary>
        /// <example> 764.783 -> 764.78 </example> 
        /// <param name="num">El numero a truncar (al acabas la funcion estara truncado)</param>
        /// <returns> Void </returns>
        static public void Truncar(ref double num)
        {
            num = Math.Truncate(num);
        }

        /// <summary>
        /// Un parámetro de tipo real y otro parámetro de tipo entero y devuelve el número tras truncar la parte decimal a los decimales indicado por el segundo parámetro.
        /// </summary>
        /// <example> 764.783 y 2 -> 764.78 </example> 
        /// <param name="num">El numero a truncar</param>
        /// <param name="decimals">El numero de decimales a conservar</param>
        /// <returns> devuelve el número tras truncar la parte decimal a los decimales indicado por el segundo parámetro </returns>
        static public double Truncar(double num, int decimals)
        {
            double n = Math.Pow(10, decimals); 
            return Math.Truncate(num*n) / n;
        }

        /// <summary>
        /// Un parámetro de tipo real, otro parámetro de tipo entero y otro parámetro de tipo real.El método almacena sobre el tercer parámetro el número tras truncar la parte decimal a los decimales indicado por el segundo parámetro.
        /// </summary>
        /// <example> 764.783 y 2 -> 764.78 </example> 
        /// <param name="num">El numero a truncar</param>
        /// <param name="decimals">El numero de decimales a conservar</param>
        /// <param name="resut">Contendra el numero ya truncado</param>
        /// <returns> Void </returns>
        static public void Truncar(double num, int decimals, out double result)
        {
            double n = Math.Pow(10, decimals);
            result = Math.Truncate(num * n) / n;
        }

        /// <summary>
        /// Un parámetro de tipo real y otro parámetro de tipo entero.Almacena sobre el primer parámetro el número tras truncar la parte decimal a los decimales indicado por el segundo parámetro.
        /// </summary>
        /// <example> 764.783 y 2 -> 764.78 </example> 
        /// <param name="num">El numero a truncar (al acabas la funcion estara truncado)</param>
        /// <param name="decimals">El numero de decimales a conservar</param>
        /// <returns> Void </returns>
        static public void Truncar(ref double num, int decimals)
        {
            double n = Math.Pow(10, decimals);
            num = Math.Truncate(num * n) / n;
        }
    }
}
